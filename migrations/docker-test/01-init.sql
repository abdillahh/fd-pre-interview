DROP DATABASE IF EXISTS fd;
CREATE DATABASE fd;

\c fd;

CREATE TABLE IF NOT EXISTS transactions (
    id SERIAL PRIMARY KEY, 
    firstName VARCHAR NOT NULL, 
    lastName VARCHAR NOT NULL, 
    email VARCHAR NOT NULL, 
    item VARCHAR NOT NULL, 
    quantity INT DEFAULT 0, 
    totalPrice INT DEFAULT 0, 
    c_at TIMESTAMP DEFAULT NOW(), 
    u_at TIMESTAMP DEFAULT NOW(), 
    deleted_at TIMESTAMP DEFAULT NULL
);

CREATE INDEX IF NOT EXISTS first_name_idx ON transactions (firstName, id);
CREATE INDEX IF NOT EXISTS last_name_idx ON transactions (lastName, id);
CREATE INDEX IF NOT EXISTS item_idx ON transactions (item, id);


INSERT INTO transactions (firstName, lastName, email, item, quantity, totalPrice)
VALUES 
('Tommy', 'Bejo', 'tommy@mail.com', 'Barang1', 2, 100000), 
('Joko', 'Widodo', 'joko@mail.com', 'Barang2', 1, 50000), 
('Jusuf', 'Kala', 'jusuf@mail.com', 'Barang3', 3, 150000), 
('Tommy', 'Bejo', 'tommy@mail.com', 'Barang1', 2, 100000), 
('Joko', 'Widodo', 'joko@mail.com', 'Barang5', 2, 100000), 
('Bradawi', 'Kinan', 'kinan@mail.com', 'Barang6', 10, 150000), 
('Kenzo', 'Purwanto', 'kenzo@mail.com', 'Barang7', 2, 100000), 
('Tami', 'Krisna', 'tami@mail.com', 'Barang8', 2, 100000), 
('Kurano', 'Bejo', 'bejo@mail.com', 'Barang9', 2, 100000), 
('Kusnadi', 'Kirin', 'kirin@mail.com', 'Barang10', 2, 100000), 
('Robert', 'Gracia', 'robert@mail.com', 'Barang3', 2, 100000),
('Kusnadi', 'Kirin', 'kirin@mail.com', 'Barang4', 2, 100000), 
('Robert', 'Ajiw', 'kunto@mail.com', 'Barang5', 2, 100000);