const TransactionModel = require('../../model/transaction.model');
const TransactionService = require('../transaction.service');
const sinon = require('sinon');
const { expect } = require('chai');

describe("Transaction Service Test", () => {
    let sandbox = sinon.createSandbox();

    afterEach(() => {
        sandbox.restore();
    });

    describe("Insert", () => {
        it("should success", async () => {
            const stubValue = {
                id: 1,
                firstName: 'hafizh',
                lastName: 'abdillah',
                email: 'hafizh@mail.com',
                quantity: 1,
                item: 'barang1',
                totalPrice: 10000
            };
            const stub = sandbox.stub(TransactionModel, 'insert').returns(stubValue);
            const spy = sandbox.spy(TransactionService, 'insert');
            const tx = await spy(stubValue.firstName, stubValue.lastName, stubValue.email, stubValue.item, stubValue.quantity, stubValue.totalPrice);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(tx.id).to.be.equal(stubValue.id);
            expect(tx.firstName).to.be.equal(stubValue.firstName);
            expect(tx.lastName).to.be.equal(stubValue.lastName);
            expect(tx.email).to.be.equal(stubValue.email);
            expect(tx.quantity).to.be.equal(stubValue.quantity);
            expect(tx.item).to.be.equal(stubValue.item);
        });

        it("should failed", async () => {
            const stub = sandbox.stub(TransactionModel, 'insert').rejects('error');
            const spy = sandbox.spy(TransactionService, 'insert');
            try {
                const tx = await spy();
                expect(stub.calledOnce).to.be.true;
                expect(spy.calledOnce).to.be.true;
                expect(tx).to.be.undefined;
            } catch (err) {
                expect(err).to.be.not.undefined;
            }
        });
    });

    describe("Update", () => {
        it("should sucess", async () => {
            const stubValue = {
                id: 1,
                firstName: 'hafizh',
                lastName: 'abdillah',
                email: 'hafizh@mail.com',
                quantity: 1,
                item: 'barang1',
                totalPrice: 10000
            };
            const stub = sandbox.stub(TransactionModel, 'update').returns(stubValue);
            const spy = sandbox.spy(TransactionService, 'update');
            const tx = await spy(stubValue.id, stubValue.firstName, stubValue.lastName, stubValue.email, stubValue.item, stubValue.quantity, stubValue.totalPrice);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(tx.id).to.be.equal(stubValue.id);
            expect(tx.firstName).to.be.equal(stubValue.firstName);
            expect(tx.lastName).to.be.equal(stubValue.lastName);
            expect(tx.email).to.be.equal(stubValue.email);
            expect(tx.quantity).to.be.equal(stubValue.quantity);
            expect(tx.item).to.be.equal(stubValue.item);
        });

        it("should failed", async () => {
            const stub = sandbox.stub(TransactionModel, 'update').rejects('error');
            const spy = sandbox.spy(TransactionService, 'update');
            try {
                const tx = await spy();
                expect(stub.calledOnce).to.be.true;
                expect(spy.calledOnce).to.be.true;
                expect(tx).to.be.undefined;
            } catch (err) {
                expect(err).to.be.not.undefined;
            }
        });

        it("should return null", async () => {
            const stub = sandbox.stub(TransactionModel, 'update').returns(null);
            const spy = sandbox.spy(TransactionService, 'update');
            const tx = await spy(0, 'something', 'something', 'mail@mail.com', 'something1', 1, 1000);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(tx).to.be.null
        });
    })

    describe("Remove", () => {
        it("should success", async () => {
            const stubValue = {
                id: 1,
                firstName: 'hafizh',
                lastName: 'abdillah',
                email: 'hafizh@mail.com',
                quantity: 1,
                item: 'barang1',
                totalPrice: 10000
            };
            const stub = sandbox.stub(TransactionModel, 'remove').returns(stubValue);
            const spy = sandbox.spy(TransactionService, 'remove');
            const tx = await spy(stubValue.id);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(tx).equal(stubValue);
        });

        it("should return null", async () => {
            const stub = sandbox.stub(TransactionModel, 'remove').returns(null);
            const spy = sandbox.spy(TransactionService, 'remove');
            const tx = await spy(9999);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(tx).to.be.null;
        });
    });

    describe("Find All", () => {
        it("should data length == 1", async () => {
            const stubValues = [
                {
                    id: 1,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 2,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 3,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 4,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 5,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }
            ];
            const limit = 5;
            const skip = 0;
            const stub = sandbox.stub(TransactionModel, 'findAll').returns(stubValues);
            const spy = sandbox.spy(TransactionService, 'findAll');
            const txs = await spy(limit, skip);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(txs).to.be.an('array');
            expect(txs).length(limit);
        });

        it("should data length == 0", async () => {
            const stubValues = [];
            const limit = 5;
            const skip = 1000;
            const stub = sandbox.stub(TransactionModel, 'findAll').returns(stubValues);
            const spy = sandbox.spy(TransactionService, 'findAll');
            const txs = await spy(limit, skip);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(txs).to.be.an('array');
            expect(txs).length(0);
        });

        it("should give default 10 pagination", async () => {
            const stubValues = [
                {
                    id: 1,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 2,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 3,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 4,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 5,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 6,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 7,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 8,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 9,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }, 
                {
                    id: 10,
                    firstName: 'hafizh',
                    lastName: 'abdillah',
                    email: 'hafizh@mail.com',
                    quantity: 1,
                    item: 'barang1',
                    totalPrice: 10000
                }
            ];
            const limit = 10;
            const stub = sandbox.stub(TransactionModel, 'findAll').returns(stubValues);
            const spy = sandbox.spy(TransactionService, 'findAll');
            const txs = await spy();
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(txs).to.be.an('array');
            expect(txs).length(limit);
        })
    });

    describe("Find All Pivot", () => {
        it("should equal <= limit", async () => {
            const stubValues = [
                {
                    email: 'hafizh@mail.com',
                    fullname: 'hafizh abdillahh', 
                    barang1: 10,
                    barang2: 0,
                    barang3: 0
                }, 
                {
                    email: 'dorman@mail.com',
                    fullname: 'hafizh dorman', 
                    barang3: 10,
                    barang2: 0,
                    barang1: 1
                }, 
                {
                    email: 'budiman@mail.com',
                    fullname: 'budi mana', 
                    barang1: 10,
                    barang2: 0,
                    barang3: 0
                }
            ];
            const limit = 3;
            const skip = 0;
            const stub = sandbox.stub(TransactionModel, 'findAllPivot').returns(stubValues);
            const spy = sandbox.spy(TransactionService, 'findAllPivot');
            const txs = await spy(limit, skip);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(txs).to.be.an('array');
            expect(txs).length.lessThanOrEqual(limit);
        });

        it("should length == 0", async () => {
            const stubValues = [];
            const limit = 10;
            const skip = 10000;
            const stub = sandbox.stub(TransactionModel, 'findAllPivot').returns(stubValues);
            const spy = sandbox.spy(TransactionService, 'findAllPivot');
            const txs = await spy(limit, skip);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(txs).to.be.an('array');
            expect(txs).length(0);
        });
    })

    describe("Find", () => {
        it ("should return tranasction", async () => {
            const stubValue = {
                id: 1,
                firstName: 'hafizh', 
                lastName: 'abdillah',
                item: 'barang1',
                quantity: 3,
                totalPrice: 1000
            };

            const stub = sandbox.stub(TransactionModel, 'find').returns(stubValue);
            const spy = sandbox.spy(TransactionService, 'find');
            const tx = await spy(stubValue.id);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(tx.id).equal(stubValue.id);
            expect(tx.firstName).equal(stubValue.firstName);
            expect(tx.lastName).equal(stubValue.lastName);
            expect(tx.quantity).equal(stubValue.quantity);
            expect(tx.totalPrice).equal(stubValue.totalPrice);
        });

        it("should return null", async () => {
            const stub = sandbox.stub(TransactionModel, 'find').returns(null);
            const spy = sandbox.spy(TransactionService, 'find');
            const tx = await spy(909090);
            expect(stub.calledOnce).to.be.true;
            expect(spy.calledOnce).to.be.true;
            expect(tx).to.be.null;
        })
    })
});