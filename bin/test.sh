cp ./test-type.env ./.env
docker-compose up -d postgres-test

WAIT_FOR_PG_ISREADY="while ! pg_ready --quite; do sleep 1; done;"
docker-compose exec postgres bash -c "$WAIT_FOR_PG_ISREADY"

echo "Running All Test"

nyc mocha '**/test/*.test.js'

echo "Tearing down all container.."
docker-compose down -v