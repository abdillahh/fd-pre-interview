const TransactionModel = require('../transaction.model');
const sinon = require("sinon");
const { expect } = require('chai');

describe("transaction.model", () => {
    let sandbox = sinon.createSandbox();


    afterEach(() => {
        sandbox.restore();
    });

    describe("Insert", () => {
        it("should success", async () => {
            const stubValue = {
                firstName: "wahyu",
                lastName: "lolo",
                email: "wahyu@mail.com",
                item: "barang1",
                quantity: 1,
                totalPrice: 10000
            };
            const spy = sandbox.spy(TransactionModel, "insert");
            const transaction = await spy(stubValue.firstName, stubValue.lastName, stubValue.email, stubValue.item, stubValue.quantity, stubValue.totalPrice);
            expect(spy.calledOnce).to.be.true;
            expect(transaction.firstName).equal(stubValue.firstName);
            expect(transaction.lastName).equal(stubValue.lastName);
            expect(transaction.email).equal(stubValue.email);
            expect(transaction.item).equal(stubValue.item);
            expect(transaction.quantity).equal(stubValue.quantity);
            expect(transaction.totalPrice).equal(stubValue.totalPrice);
        });

        it("should failed insert", async () => {
            const spy =  sandbox.spy(TransactionModel, 'insert');
            try {
                const transaction = await spy();
                expect(transaction).to.be.undefined;
                expect(spy.calledOnce).to.be.true;
                expect(transaction);
            } catch (err) {
                expect(err).to.not.undefined;
            }
        })
    });

    describe("Remove", () => {
        it("should success", async () => {
            const id = 1;
            const spy = sandbox.spy(TransactionModel, 'remove');
            const tx = await spy(id);
            expect(spy.calledOnce).to.be.true;
            expect(tx).to.haveOwnProperty('firstName');
            expect(tx).to.haveOwnProperty('lastName');
            expect(tx).to.haveOwnProperty('email');
            expect(tx).to.haveOwnProperty('quantity');
            expect(tx).to.haveOwnProperty('item');
            expect(tx).to.haveOwnProperty('totalPrice');
        })

        it("nothing remove", async () => {
            const id = 10000;
            const spy = sandbox.spy(TransactionModel, 'remove');
            const tx = await spy(id);
            expect(spy.calledOnce).to.be.true;
            expect(tx).to.be.null;
        })
    });


    describe("Update", () => {
        it("should update one", async () => {
            const stubValue = {
                id: 3,
                firstName: 'Budi',
                lastName: 'Anduk',
                email: 'anduk@mail.com',
                item: 'barang4',
                quantity: 2,
                totalPrice: 30000
            };
            const spy = sandbox.spy(TransactionModel, 'update');
            const tx = await spy(stubValue.id, stubValue.firstName, stubValue.lastName, stubValue.email, stubValue.item, stubValue.quantity, stubValue.totalPrice);
            expect(spy.calledOnce).to.be.true;
            expect(tx.firstName).to.equal(stubValue.firstName);
            expect(tx.lastName).to.equal(stubValue.lastName);
            expect(tx.email).to.equal(stubValue.email);
            expect(tx.quantity).to.equal(stubValue.quantity);
            expect(tx.totalPrice).to.equal(stubValue.totalPrice);
        });

        it("should update none", async () => {
            const stubValue = {
                id: 9999,
                firstName: 'Budi',
                lastName: 'Anduk',
                email: 'anduk@mail.com',
                item: 'barang4',
                quantity: 2,
                totalPrice: 30000
            };
            const spy = sandbox.spy(TransactionModel, 'update');
            const tx = await spy(stubValue.id, stubValue.firstName, stubValue.lastName, stubValue.email, stubValue.item, stubValue.quantity, stubValue.totalPrice);
            expect(spy.calledOnce).to.be.true;
            expect(tx).to.be.null;
        });

        it("should failed", async () => {
            const spy = sandbox.spy(TransactionModel, 'update');
            try {
                const tx = await spy();
                expect(spy.calledOnce).to.be.true;
                expect(tx).to.be.undefined;
            } catch (err) {
                expect(err).to.be.not.null;
            }
        })
    })

    describe("Find All", () =>  {
        it("should have length of 1", async () => {
            const limit = 1;
            const skip = 0;
            const spy = sandbox.spy(TransactionModel, 'findAll');
            const transactions = await spy(limit, skip);
            expect(spy.calledOnce).to.be.true;
            expect(transactions).length(limit);
        })

        it("should have length of 0", async () => {
            const limit = 10;
            const skip = 1000;
            const spy = sandbox.spy(TransactionModel, 'findAll');
            const transactions = await spy(limit, skip);
            expect(spy.calledOnce).to.be.true;
            expect(transactions).length(0);
        });

        it("should have properties", async () => {
            const limit = 5;
            const skip = 0;
            const spy = sandbox.spy(TransactionModel, 'findAll');
            const transactions = await spy(limit, skip);
            expect(spy.calledOnce).to.be.true;
            expect(transactions).to.be.an('array');
            expect(transactions).length(limit);
            transactions.forEach(tx => {
                expect(tx).haveOwnProperty('firstName');
                expect(tx).haveOwnProperty('lastName');
                expect(tx).haveOwnProperty('email');
                expect(tx).haveOwnProperty('item');
                expect(tx).haveOwnProperty('quantity');
                expect(tx).haveOwnProperty('totalPrice');
            })
        }); 
    });

    describe("Find All Pivot", () => {
        it("should have length of 1", async () => {
            const limit = 1;
            const skip = 0;
            const spy = sandbox.spy(TransactionModel, 'findAllPivot');
            const tx = await spy(limit, skip);
            expect(spy.calledOnce).to.be.true;
            expect(tx).length(limit);
        });

        it("should have length of 0", async () => {
            const limit = 1;
            const skip = 1000;
            const spy = sandbox.spy(TransactionModel, 'findAllPivot');
            const tx = await spy(limit, skip);
            expect(spy.calledOnce).to.be.true;
            expect(tx).length(0);
        }); 

        it("has own properites", async () => {
            const limit = 3;
            const skip = 0;
            const spy = sandbox.spy(TransactionModel, 'findAllPivot');
            const txs = await spy(limit, skip);
            expect(spy.calledOnce).to.be.true;
            txs.forEach(tx => {
                expect(tx).has.ownProperty('fullName');
                expect(tx).has.ownProperty('email');
            })
        })
    });

    describe("Find", () => {
        it("should return transaction", async () => {
            const id = 3;
            const spy = sandbox.spy(TransactionModel, 'find');
            const tx = await spy(id);
            expect(spy.calledOnce).to.be.true;
            expect(tx.id).equal(id);
        });

        it("should return null", async () => {
            const id = 9999;
            const spy = sandbox.spy(TransactionModel, 'find');
            const tx = await spy(id);
            expect(spy.calledOnce).to.be.true;
            expect(tx).to.be.null;
        });
    });
});