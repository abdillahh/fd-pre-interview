const { pg } = require('../database');
const { pgTable } = require('../common/constant');


const findAll = async (limit, skip) => {
    try {
        const query = `
            SELECT * FROM ${pgTable.transaction} WHERE deleted_at IS NULL
            ORDER BY c_at DESC
            LIMIT $1 OFFSET $2;
        `;
        const rows = await pg.query(query, [limit, skip]);
        const data = rows.map(r => _transformer(r));
        return Promise.resolve(data);
    } catch (err) {
        return Promise.reject(err);
    }
};

const findAllPivot = async (limit, skip) => {
    try {
        const query = `
        WITH total_trans as (
            select 
            email, 
            concat(firstName, ' ', lastName) as fullName, 
            item, 
            sum(quantity) as total 
            from ${pgTable.transaction} t2 WHERE deleted_at IS NULL
            group by email, item, fullName
        )
        select email, fullName, 
            jsonb_object_agg(item, total) as items
        from total_trans
        GROUP BY email, fullName 
        LIMIT $1 OFFSET $2;
    `;
    const rows = await pg.query(query, [limit, skip]);
    let data = _pivotTransformer(rows);
    return Promise.resolve(data);
    } catch (err) {
        return Promise.reject(err);
    }
}

const insert = async (firstName, lastName, email, item, quantity, totalPrice) => {
    try {
        const query = `
            INSERT INTO ${pgTable.transaction}
            (firstName, lastName, email, item, quantity, totalPrice) 
            VALUES ($1, $2, $3, $4, $5, $6) RETURNING *;
        `;
        const rows = await pg.query(query, [firstName, lastName, email, item, quantity, totalPrice]);
        const data = rows && rows.length > 0 ? _transformer(rows[0]) : null;
        return Promise.resolve(data);
    } catch (err) {
        return Promise.reject(err);
    }
}

const remove = async (id) => {
    try {
        const query = `
            UPDATE ${pgTable.transaction} SET deleted_at = NOW(), u_at = NOW()
            WHERE id = $1 RETURNING *;
        `;
        const rows = await pg.query(query, [id]);
        const data = rows && rows.length > 0 ? _transformer(rows[0]) : null;
        return Promise.resolve(data);
    } catch (err) {
        return Promise.reject(err);
    }
}

const find = async (id) => {
    try {
        const query = `
            SELECT * FROM ${pgTable.transaction}
            WHERE id = $1 AND deleted_at IS NULL;
        `;
        const rows = await pg.query(query, [id]);
        const data = rows && rows.length > 0 ? _transformer(rows[0]) : null;
        return Promise.resolve(data);
    } catch (err) {
        return Promise.reject(err);
    }
}

const update = async (id, firstName, lastName, email, item, quantity, totalPrice) => {
    try {
        const query = `
            UPDATE ${pgTable.transaction}
            SET 
            firstName = $2, 
            lastName = $3, 
            email = $4, 
            item = $5, 
            quantity = $6, 
            totalPrice = $7 
            WHERE id = $1 AND deleted_at IS NULL RETURNING *;
        `;
        const rows = await pg.query(query, [id, firstName, lastName, email, item, quantity, totalPrice]);
        const data = rows && rows.length > 0 ? _transformer(rows[0]) : null;
        return Promise.resolve(data);
    } catch (err) {
        return Promise.reject(err);
    }
}

const _pivotTransformer = (pivots) => {
    const itemsKey = [];
    pivots.forEach(p => {
        p.fullName = p.fullname;
        delete p.fullname;
        for(let key in p.items) {
            let idx = itemsKey.findIndex(k => k == key);
            p[key] = p.items[key];
            if (idx == -1) {
                itemsKey.push(key);
            }
        }
    });

    itemsKey.forEach(k => {
        pivots.forEach(p => {
            if (!p.hasOwnProperty(k)) {
                p[k] = 0;
            }
            delete p.items;
        })
    })
    return pivots;
}

const _transformer = (rawModel) => {
    return {
        id: rawModel.id, 
        firstName: rawModel.firstname, 
        lastName: rawModel.lastname, 
        email: rawModel.email,
        item: rawModel.item, 
        quantity: rawModel.quantity, 
        totalPrice: rawModel.totalprice, 
        c_at: rawModel.c_at,
        u_at: rawModel.u_at
    }
}

module.exports = {
    insert, 
    update, 
    remove, 
    findAll, 
    findAllPivot, 
    find
}