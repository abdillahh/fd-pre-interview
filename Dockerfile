# syntax=docker/dockerfile:1

FROM node:14.17.0

WORKDIR /src/app

COPY ["package.json", "package-lock.json", "./"]

RUN npm install --production

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]