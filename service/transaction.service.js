const Model = require("../model");

const findAll = async (limit = 10, skip = 0) => {
    return await Model.transaction.findAll(limit, skip);
}

const findAllPivot = async (limit = 10, skip = 0) => {
    return await Model.transaction.findAllPivot(limit, skip);
}

const insert = async (firstName, lastName, email, item, quantity, totalPrice) => {
    return await Model.transaction.insert(firstName, lastName, email, item, quantity, totalPrice);
}

const update = async (id, firstName, lastName, email, item, quantity, totalPrice) => {
    return await Model.transaction.update(id, firstName, lastName, email, item, quantity, totalPrice)
};

const remove = async (id) => {
    return await Model.transaction.remove(id);
}

const find = async (id) => {
    return await Model.transaction.find(id);
}

module.exports = {
    findAll, 
    findAllPivot, 
    insert, 
    update, 
    remove, 
    find
}