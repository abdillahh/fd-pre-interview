const router = require('express').Router();
const { transactionService } = require('../service');
const ValidateJs = require("validate.js");

router.get('/transactions', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit || 10);
        const skip = parseInt(req.query.skip || 0);
        const results = await transactionService.findAll(limit, skip);
        const data = results.map(r => _transformer(r));
        res.render('main', {data});
    } catch (err) {
        res.render('error', {code: 500, message: err.message});
    }
});


router.get('/transactions/pivot', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit || 10);
        const skip = parseInt(req.params.skip || 0);
        const results = await transactionService.findAllPivot(limit, skip);
        let headersEjs = [];

        if (results.length > 0) {
            headersEjs = _generatePivotTableHeader(results[0]);
        }
        res.render('pivot', {data: results, headersEjs})
    } catch (err) {
        res.render('error', {code: 500, message: err.message});
    }
});

router.post('/transactions', async (req, res) => {
    try {
        const constraints = {
            firstName: {presence: true},
            lastName: {presence: true}, 
            item: {presence: true},
            quantity: {presence: true, numericality: true},
            email: {presence: true, email: true},
            totalPrice: {presence: true, numericality: true}
        }
        const validation = ValidateJs.validate(req.body, constraints, {fullMessages: false});

        if (validation) {
            res.render('error', {code: 400, message: JSON.stringify(validation)});
            return;
        }

        const {firstName, lastName, email, item, quantity, totalPrice} = req.body;
        await transactionService.insert(firstName, lastName, email, item, quantity, totalPrice);
        res.status(301).redirect('/api/v1/transactions');
        return;
    } catch (err) {
        res.render('error', {code: 500, message: err.message})
    }
});

router.delete('/transactions/:id', async (req, res) => {
    try {
        const tx = await transactionService.remove(req.params.id);
        if (!tx) {
            res.render('error', {code: 404, message: 'Not Found'});
            return;
        }
        res.status(200).json({status: 'redirect', url: '/'})
        return;
    } catch (err) {
        res.render('error', {code: 500, message: err.message});
    }
})

router.get('/transactions/:id', async (req, res) => {
    try {
        const tx = await transactionService.find(req.params.id);
        if (!tx) {
            res.render('error', {code: 404, message: 'Not Found'});
            return;
        }
        res.render('edit', {data: tx});
    } catch (err) {
        res.render('error', {code: 500, message});
    }
});

router.put('/transactions/:id', async (req, res) => {
    const constraints = {
        firstName: {presence: true},
        lastName: {presence: true}, 
        item: {presence: true},
        quantity: {presence: true, numericality: true},
        email: {presence: true, email: true},
        totalPrice: {presence: true, numericality: true}
    }
    const validation = ValidateJs.validate(req.body, constraints, {fullMessages: false});

    if (validation) {
        res.status(400).json({validation: validation})
        return;
    }

    const {firstName, lastName, email, quantity, item, totalPrice} = req.body;
    const tx = await transactionService.update(req.params.id, firstName, lastName, email, item, quantity, totalPrice);
    if (!tx) {
        res.status(404).json({});
        return;
    }
    res.status(200).json({status: 'redirect', url: '/'})
});

const _generatePivotTableHeader = (data) => {
    let list = [];
    for(let key in data) {
        list.push(key);
    };
    return list;
}


const _transformer = (data) => {
    return {
        id: data.id,
        fullName: data.firstName + ' ' + data.lastName,
        item: data.item,
        email: data.email, 
        quantity: data.quantity,
        totalPrice: data.totalPrice,
        c_at: data.c_at,
        u_at: data.u_at
    }
}

module.exports = router;