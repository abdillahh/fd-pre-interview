const promise = require("bluebird");
const pgPromise = require("pg-promise");
const config = require("../common/config");

const initOptions = {
    promiseLib: promise
};

const pgp = pgPromise(initOptions);
const db = pgp(config.psql);

module.exports = db;