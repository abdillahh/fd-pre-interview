require('dotenv').config();

module.exports = {
    psql: process.env.NODE_ENV == 'heroku' ? {
        connectionString: process.env.DATABASE_URL,
        ssl: {
            rejectUnauthorized: false
        }
    } : {
        host: process.env.PSQL_HOST || "localhost",
        port: process.env.NODE_ENV == 'test_docker' ? 2345 : process.env.PSQL_PORT || 5432,
        database: process.env.PSQL_DB || "db",
        user: process.env.PSQL_USER || "guest",
        password: process.env.PSQL_PASSWORD || ""
    }
}