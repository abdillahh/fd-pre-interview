DROP DATABASE IF EXISTS fd;
CREATE DATABASE fd;

\c fd;

CREATE TABLE IF NOT EXISTS transactions (
    id SERIAL PRIMARY KEY, 
    firstName VARCHAR NOT NULL, 
    lastName VARCHAR NOT NULL, 
    email VARCHAR NOT NULL, 
    item VARCHAR NOT NULL, 
    quantity INT DEFAULT 0, 
    totalPrice INT DEFAULT 0, 
    c_at TIMESTAMP DEFAULT NOW(), 
    u_at TIMESTAMP DEFAULT NOW(), 
    deleted_at TIMESTAMP DEFAULT NULL
);

CREATE INDEX IF NOT EXISTS first_name_idx ON transactions (firstName, id);
CREATE INDEX IF NOT EXISTS last_name_idx ON transactions (lastName, id);
CREATE INDEX IF NOT EXISTS item_idx ON transactions (item, id);