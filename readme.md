# FD - Pre Inrterview

This the documentation of this project

### API
|Method | Endpoint | Description | params| body|
|----|----|-------| ----|----|
|GET | /| Will Redirect To /transactions| none|none|
|GET|/transactions |Fetch all transactions|limit (optional), skip (optional)|none|
|GET|/transactions/pivot|Fet all pivoted transactions| limit, skip|none|
|POST|/transactions| Insert New Transactions| none |firstName: string, lastName: string, email: email, item: string, quantity: number, totalPrice: number|
|PUT|/transactions/:id| Update Existing Transaction Data| id (mandatory)|firstName: string, lastName: string, email: email, item: string, quantity: number, totalPrice: number|
|DELETE|/transactions/:id|Soft Delete Existing Transaction|id (mandatory|none|

## Installation

stack:
 - Node v14
 - Express js
 - Postgres
 - Docker

```bash
git clone https://gitlab.com/abdillahh/fd-pre-interview.git
```

```bash
cd fd-pre-interview/ && npm install
```


## Development
```bash
npm run start:dev
```

## Docker - Usage

### Test - Local

```bash
touch .env
```
Set this env variables with your local configuration

```bash
npm run test:dev
```

### Test - Deployment

```bash
touch test-type.env
```
setup with your test env variables.

```bash
npm run test
```

### Run Application
```bash
touch ./example.env ./.env
```
Update your env variables.

```bash
docker-compose up app postgres
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)