require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const path = require('path');
const { transactionController } = require('./controller');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.set('views', path.join(__dirname, 'view'));
app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/public'));


app.get('/', (req, res) => {
    res.redirect('/api/v1/transactions')
});

app.use('/api/v1', transactionController);

app.listen(port, () => {
    console.log("Application Running on Port", port);
})